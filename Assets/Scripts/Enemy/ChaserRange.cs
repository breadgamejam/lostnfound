﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaserRange : MonoBehaviour
{
    CircleCollider2D chaserRangeCircleCollider2D;
    ChaserEnemy chaserEnemy;

    // Start is called before the first frame update
    void Start()
    {
        chaserEnemy = gameObject.GetComponentInParent<ChaserEnemy>();
    }

    // Update is called once per frame
    // void Update()
    // {
        
    // }

    private void OnTriggerEnter2D(Collider2D other) {
        // (other.gameObject.tag);
        if(other.gameObject.tag == "Player") {
            chaserEnemy.isInRange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.tag == "Player") {
            chaserEnemy.isInRange = false;
        }
    }
}
