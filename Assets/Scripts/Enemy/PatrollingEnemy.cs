﻿using UnityEngine;

public class PatrollingEnemy : MonoBehaviour
{
    [SerializeField] private Vector2[] positions;
    [SerializeField] private float speed = 10.0f;
    private int positionIndexToReach = 0;
    private Rigidbody2D rb;

    void Start() {
        rb = gameObject.GetComponent<Rigidbody2D>();    
    }

    void Update()
    {
        Vector2 currentPosition = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);

        if(Mathf.Abs(currentPosition.x - positions[positionIndexToReach].x) <= 0.1f) {
            positionIndexToReach = (positionIndexToReach + 1) % positions.Length;
        }
        
        Vector2 direction = (positions[positionIndexToReach] - currentPosition).normalized;
        direction.x = Mathf.Sign(direction.x);
        direction.y = 0.0f;
        rb.velocity = direction * speed;
        gameObject.GetComponentInChildren<SpriteRenderer>().flipX = direction.x > 0;
    }
}
