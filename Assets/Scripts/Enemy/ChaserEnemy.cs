﻿using UnityEngine;

// Chaser should start chasing when in range
//  and a direct ray can be cast from enemy to player
public class ChaserEnemy : MonoBehaviour
{
    BoxCollider2D boxCollider2D;
    [SerializeField] public bool isInRange = false;
    [SerializeField] private float thrust = 10.0f;
    [SerializeField] private float startSpeed = 2.0f;
    [SerializeField] private float maxSpeed = 5.0f;
    private float speedMultiplicationFactor = 1.0f;
    [SerializeField] private float speedMultiplicationFactorDelta = 0.5f;
    private bool isChasing = false;
    Rigidbody2D rb;
    // [SerializeField] float chaseEvadeTimerInMs = 2000;
    private GameObject player;
    Vector2 initialPosition;
    RaycastHit2D raycast;

    // Start is called before the first frame update
    void Start()
    {
        initialPosition = (Vector2)transform.position;
        boxCollider2D = gameObject.GetComponent<BoxCollider2D>();
        rb = gameObject.GetComponent<Rigidbody2D>();
        player = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        // if(isChasing) {

        // }
    }

    void FixedUpdate()
    {
        Vector2 heading;
        if (isInRange && Physics2D.Raycast((Vector2)gameObject.transform.position, (Vector2)player.transform.position - (Vector2)gameObject.transform.position).collider.gameObject.tag == "Player")
        {
            // raycast = Physics2D.Raycast((Vector2)gameObject.transform.position, (Vector2)player.transform.position);
            isChasing = true;
            speedMultiplicationFactor += speedMultiplicationFactorDelta;
            heading = player.transform.position - gameObject.transform.position;
            // Vector2 direction = heading / heading.magnitude;
            rb.velocity = (heading.normalized * startSpeed * speedMultiplicationFactor);
        }
        else
        {
            if(isChasing) {
                isChasing = false;
                speedMultiplicationFactor = 1.0f;
            }
            // isChasing = false;
            heading = initialPosition - (Vector2)gameObject.transform.position;
            if (heading.sqrMagnitude > 0.1)
            {
                speedMultiplicationFactor += speedMultiplicationFactorDelta;
                rb.velocity = (heading.normalized * startSpeed * speedMultiplicationFactor);
            } else {
                speedMultiplicationFactor = 1.0f;
                // reset velocity to zero if back in its initial position
                rb.velocity = Vector2.zero;
            }
        }
        // rb.velocity can be 0, 0 in here
        // Don't increase it in here
        rb.velocity = (rb.velocity.magnitude > maxSpeed) ? maxSpeed * heading.normalized : rb.velocity;
        gameObject.GetComponent<SpriteRenderer>().flipX = heading.normalized.x > 0;
    }
}
