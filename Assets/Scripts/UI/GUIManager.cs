﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game_Jam.Assets.Scripts.Abilities;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour
{
    public GameObject pausePanel, gameOverPanel, levelCompletePanel, abilitySwapPanel, abilitySlotPanel;
    public TextMeshProUGUI collectedBonusPoints, abilityNameUI, abilityInfoUI, levelCompleteInfo;

    public Image abilitySwapButton1, abilitySwapButton2, abilitySwapButton3;
    public Image abilityDisplayButton1, abilityDisplayButton2, abilityDisplayButton3;
    public TextMeshProUGUI abilityDisplayButtonText1, abilityDisplayButtonText2, abilityDisplayButtonText3;
    public Sprite jump, climb, melee, dash;
    public static GUIManager instance;
    private Action<AbilityType, int> toExecute;

    private void Start()
    {
        pausePanel.SetActive(false);
        gameOverPanel.SetActive(false);
        levelCompletePanel.SetActive(false);
        abilitySwapPanel.SetActive(false);
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void swapAbility(AbilityType ability, string abilityInfo, Action<AbilityType, int> execute)
    {
        abilitySlotPanel.SetActive(false);
        abilitySwapPanel.SetActive(true);

        abilityNameUI.SetText(ability.ToString().ToUpper());
        abilityInfoUI.SetText(abilityInfo);
        toExecute = execute;
        Time.timeScale = 0;
    }

    public void abilitySelect(int index)
    {
        if (GameManager.getInstance().abilitySlotsForGui[index] != AbilityType.None && toExecute != null)
        {
            toExecute(GameManager.getInstance().abilitySlotsForGui[index], index);
            toExecute = null;
            abilitySlotPanel.SetActive(true);
            abilitySwapPanel.SetActive(false);
            Time.timeScale = 1;
        }
    }

    public void cancelSlotSwap()
    {
        if (toExecute != null)
        {
            toExecute(AbilityType.None, -1);
            toExecute = null;
            abilitySlotPanel.SetActive(true);
            abilitySwapPanel.SetActive(false);
            Time.timeScale = 1;
        }
    }

    private void Update()
    {
        collectedBonusPoints.SetText(GameManager.getInstance().currentLevelBonus + "/" + GameManager.getInstance().currentLevelBonusMax + " Collected");
        levelCompleteInfo.SetText(GameManager.getInstance().currentLevelBonus + "/" + GameManager.getInstance().currentLevelBonusMax + " Collected");
        if (GameManager.getInstance().abilitySlotsForGui[0] != AbilityType.None)
        {
            abilitySwapButton1.transform.parent.gameObject.SetActive(true);
            abilitySwapButton1.sprite = getImageForSlot(0);
        }
        else
        {
            abilitySwapButton1.transform.parent.gameObject.SetActive(false);
        }
        if (GameManager.getInstance().abilitySlotsForGui[1] != AbilityType.None)
        {
            abilitySwapButton2.transform.parent.gameObject.SetActive(true);
            abilitySwapButton2.sprite = getImageForSlot(1);
        }
        else
        {
            abilitySwapButton2.transform.parent.gameObject.SetActive(false);
        }
        if (GameManager.getInstance().abilitySlotsForGui[2] != AbilityType.None)
        {
            abilitySwapButton3.transform.parent.gameObject.SetActive(true);
            abilitySwapButton3.sprite = getImageForSlot(2);
        }
        else
        {
            abilitySwapButton3.transform.parent.gameObject.SetActive(false);
        }

        if (GameManager.getInstance().abilitySlotsForGui[0] != AbilityType.None)
        {
            abilityDisplayButton1.transform.parent.gameObject.SetActive(true);
            abilityDisplayButton1.sprite = getImageForSlot(0);
        }
        else
        {
            abilityDisplayButton1.transform.parent.gameObject.SetActive(false);
        }
        if (GameManager.getInstance().abilitySlotsForGui[1] != AbilityType.None)
        {
            abilityDisplayButton2.transform.parent.gameObject.SetActive(true);
            abilityDisplayButton2.sprite = getImageForSlot(1);
        }
        else
        {
            abilityDisplayButton2.transform.parent.gameObject.SetActive(false);
        }
        if (GameManager.getInstance().abilitySlotsForGui[2] != AbilityType.None)
        {
            abilityDisplayButton3.transform.parent.gameObject.SetActive(true);
            abilityDisplayButton3.sprite = getImageForSlot(2);
        }
        else
        {
            abilityDisplayButton3.transform.parent.gameObject.SetActive(false);
        }

        if (GameManager.getInstance().abilitySlotsForGui[0] != AbilityType.None)
        {
            abilityDisplayButtonText1.gameObject.SetActive(true);
            abilityDisplayButtonText1.text = getTextForSlot(0);
        }
        else
        {
            abilityDisplayButtonText1.gameObject.SetActive(false);
        }
        if (GameManager.getInstance().abilitySlotsForGui[1] != AbilityType.None)
        {
            abilityDisplayButtonText2.gameObject.SetActive(true);
            abilityDisplayButtonText2.text = getTextForSlot(1);
        }
        else
        {
            abilityDisplayButtonText2.gameObject.SetActive(false);
        }
        if (GameManager.getInstance().abilitySlotsForGui[2] != AbilityType.None)
        {
            abilityDisplayButtonText3.gameObject.SetActive(true);
            abilityDisplayButtonText3.text = getTextForSlot(2);
        }
        else
        {
            abilityDisplayButtonText3.gameObject.SetActive(false);
        }
    }

    public void Pause()
    {
        pausePanel.SetActive(GameManager.getInstance().PauseGame());
    }

    private Sprite getImageForSlot(int index)
    {
        switch (GameManager.getInstance().abilitySlotsForGui[index])
        {
            case AbilityType.Jump:
                return jump;
            case AbilityType.Dash:
                return dash;
            case AbilityType.MeleeAttack:
                return melee;
            case AbilityType.Climb:
                return climb;
        }
        return null;
    }
    private string getTextForSlot(int index)
    {
        switch (GameManager.getInstance().abilitySlotsForGui[index])
        {
            case AbilityType.Jump:
                return "SPACE";
            case AbilityType.Dash:
                return "SHIFT";
            case AbilityType.MeleeAttack:
                return "E";
            case AbilityType.Climb:
                return "C";
        }
        return null;
    }

    public void ShowGameOverPanel()
    {
        gameOverPanel.SetActive(true);
    }

    public void LoadScene(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void Restart()
    {
        LoadScene(GameManager.getInstance().currentLevel + 1);
    }

    public void NextLevel()
    {
        if (GameManager.getInstance().currentLevel >= 2)
            return;
        LoadScene(GameManager.getInstance().currentLevel + 2);
    }
}