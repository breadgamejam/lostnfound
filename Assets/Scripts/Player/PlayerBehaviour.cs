﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    [HideInInspector] public BoxCollider2D boxCollider2D;
    [HideInInspector] public Rigidbody2D rb;
    public LayerMask platformLayerMask;
    [SerializeField] private LayerMask slidingLayers;

    public LayerMask climbableLayers;
    public LayerMask anyLayer;
    public LayerMask airLayer;
    public float interactionRadius = 1f;
    [HideInInspector] public float xVelocityCoefficient = 1f;
    public float delayedKillInteractionTime = 1f;
    public float delayedKillInteractionTimeEnemy = 1f;
    
    void Start()
    {
        boxCollider2D = gameObject.GetComponent<BoxCollider2D>();
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    void Update() {
        if (Input.GetButton("Horizontal")) {
            gameObject.GetComponent<SpriteRenderer>().flipX = Input.GetAxisRaw("Horizontal") < 0;
        }
        SlideOnInteraction();
    }

    private void SlideOnInteraction() {
        Collider2D[] hitslidingLayers = Physics2D.OverlapCircleAll(gameObject.transform.position, interactionRadius, slidingLayers);
        if (hitslidingLayers.Length > 0) xVelocityCoefficient = 0f; else xVelocityCoefficient = 1f;
    }

    public bool IsGrounded() {
        RaycastHit2D raycastHit2D = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.down, 0.1f, platformLayerMask);
        return raycastHit2D.collider != null;
    }

    public bool IsAir() {
        RaycastHit2D raycastHit2D = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.down, 0.1f, airLayer);
        return raycastHit2D.collider == null;
    }

    void OnDrawGizmosSelected() {
        if (gameObject.transform.position == null) return;

        Gizmos.DrawWireSphere(gameObject.transform.position, interactionRadius);
        Gizmos.DrawWireCube(gameObject.transform.position, new Vector3(interactionRadius, interactionRadius * 2, 0.0f));
    }
}
