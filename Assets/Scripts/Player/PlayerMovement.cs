﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game_Jam.Assets.Scripts.Constants;
using Game_Jam.Assets.Scripts.Abilities;

public class PlayerMovement : MonoBehaviour
{
    [HideInInspector] public PlayerBehaviour playerBehaviour;
    [HideInInspector] public Animator playerAnimator;
    [HideInInspector] public bool isAnimationPlaying = false;
    [HideInInspector] public bool isJumping = false;
    [HideInInspector] public bool isDashEnabled = false;
    [HideInInspector] public bool isPunching = false;
    [HideInInspector] public bool playerShouldDie = false;
    [HideInInspector] public PlayerAnimationState currentAnimationState = PlayerAnimationState.Idle;
    [HideInInspector] public PlayerAnimationState previousAnimationState = PlayerAnimationState.Idle;
    [HideInInspector] public Dictionary<string, float> animTimeMap;
    [HideInInspector] public int direction;
    private bool interactedWithDelayedKillOnce = false;
    private bool interactedWithEnemyOnce = false;
    private float timer = 0;

    void Start() {
        Time.timeScale = 1;
        playerBehaviour = gameObject.GetComponent<PlayerBehaviour>();
        playerAnimator = gameObject.GetComponent<Animator>();
        direction = 1;
        playerShouldDie = false;
        interactedWithDelayedKillOnce = false;
        animTimeMap = new Dictionary<string, float>();
        RuntimeAnimatorController ac = playerAnimator.runtimeAnimatorController;
        for (int i = 0; i < ac.animationClips.Length; i++) {
            animTimeMap[ac.animationClips[i].name] = ac.animationClips[i].length;
        }
    }

    void Update() {
        AnimationUpdate();

        if (interactedWithDelayedKillOnce) {
            if (interactedWithEnemyOnce) {
                if (timer > playerBehaviour.delayedKillInteractionTimeEnemy) {
                    playerShouldDie = true;
                }
            } else {
                if (timer > playerBehaviour.delayedKillInteractionTime) {
                    playerShouldDie = true;
                }
            }
        }
        timer += Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        switch (other.gameObject.tag) {
            case "InstantKill":
                playerShouldDie = true;
                break;
            case "Enemy":
                interactedWithDelayedKillOnce = true;
                interactedWithEnemyOnce = true;
                timer = 0;
                break;
        }
    }

    private void OnCollisionExit2D(Collision2D other) {
        switch (other.gameObject.tag) {
            case "Enemy":
                interactedWithDelayedKillOnce = false;
                interactedWithEnemyOnce = false;
                timer = 0;
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        switch (other.gameObject.tag) {
            case "DelayedKill":
                interactedWithDelayedKillOnce = true;
                timer = 0;
                break;
        }
    }
    private void OnTriggerExit2D(Collider2D other) {
        switch (other.gameObject.tag) {
            case "DelayedKill":
                interactedWithDelayedKillOnce = false;
                timer = 0;
                break;
        }
    }

    private void AnimationUpdate() {
        float x = playerBehaviour.rb.velocity.x;
        float y = playerBehaviour.rb.velocity.y;
        playerAnimator.speed = 1f;
        previousAnimationState = currentAnimationState;
        Collider2D[] hitClimbableObjects = Physics2D.OverlapCircleAll(gameObject.transform.position, playerBehaviour.interactionRadius, playerBehaviour.climbableLayers);

        if (canInterruptAnimation(previousAnimationState) || !isAnimationPlaying) {
            if (x > 0f) {
                currentAnimationState = PlayerAnimationState.Walk;
            } else if (x < 0f) {
                currentAnimationState = PlayerAnimationState.Walk;
            } else {
                currentAnimationState = PlayerAnimationState.Idle;
            }

                                                Debug.Log(x);
                                                Debug.Log(y);

            if (y > 0f) {
                switch(currentAnimationState) {
                    case PlayerAnimationState.Walk:
                        if (isJumping) {
                            isAnimationPlaying = false;
                            currentAnimationState = PlayerAnimationState.Jump;
                        } else if (x < 0.01f && x > -0.01f) {
                            isAnimationPlaying = false;
                            currentAnimationState = PlayerAnimationState.ClimbUp;
                        }
                        break;
                    default:   
                        if ((hitClimbableObjects.Length > 0) && Input.GetButton("Climb")) {
                            isAnimationPlaying = false;
                            currentAnimationState = PlayerAnimationState.ClimbUp;
                        } else if (isJumping) {
                            isAnimationPlaying = false;
                            currentAnimationState = PlayerAnimationState.Jump;
                        } 
                        break;
                }
            } else if (y < 0f) {
                if ((hitClimbableObjects.Length > 0) && Input.GetButton("Climb")) currentAnimationState = PlayerAnimationState.ClimbDown; else currentAnimationState = PlayerAnimationState.FreeFall;
            } else {
                isAnimationPlaying = false;
                if (previousAnimationState == PlayerAnimationState.FreeFall) {
                    currentAnimationState = PlayerAnimationState.Land;
                } else if (hitClimbableObjects.Length > 0 && ((currentAnimationState == PlayerAnimationState.Idle && Input.GetButton("Climb")))) {
                    currentAnimationState = PlayerAnimationState.StcukOnWall;
                }
            }
        }

        if (isPunching && (previousAnimationState != PlayerAnimationState.MeeleAttack && previousAnimationState != PlayerAnimationState.JumpMeeleAttack)) {
            currentAnimationState = PlayerAnimationState.MeeleAttack;
            if (playerBehaviour.IsAir()) {
                currentAnimationState = PlayerAnimationState.JumpMeeleAttack;
            }
            isAnimationPlaying = false; // Special ability overrides all other animatioins
        }

        if (playerShouldDie) {
            playerBehaviour.rb.velocity = Vector2.zero;
            currentAnimationState = PlayerAnimationState.Death;
            isAnimationPlaying = false; // Special ability overrides all other animatioins, GameOver after this
        }

        if (!isAnimationPlaying) {
            switch(currentAnimationState) {
                case PlayerAnimationState.Idle:
                    playerAnimator.Play("anim_player_idle");
                    break;
                case PlayerAnimationState.Walk:
                    if (isDashEnabled) {
                        playerAnimator.Play("anim_player_dash");
                    } else {
                        playerAnimator.Play("anim_player_walk");
                    }
                    break;
                case PlayerAnimationState.ClimbUp:
                    playerAnimator.Play("anim_player_climb_up");
                    break;
                case PlayerAnimationState.ClimbDown:
                    playerAnimator.Play("anim_player_climb_down");
                    break;
                case PlayerAnimationState.StcukOnWall:
                    playerAnimator.Play("anim_player_climb_up");
                    playerAnimator.speed = 0f;
                    break;
                case PlayerAnimationState.FreeFall:
                    playerAnimator.Play("anim_player_falling");
                    break;
                case PlayerAnimationState.Jump:
                    playerAnimator.Play("anim_player_jump");
                    isAnimationPlaying = true;
                    StartCoroutine(runAfterSeconds(animTimeMap["anim_player_jump"], () => {
                        isAnimationPlaying = false;
                    }));
                    break;
                case PlayerAnimationState.Land:
                    playerAnimator.Play("anim_player_land");
                    isAnimationPlaying = true;
                    StartCoroutine(runAfterSeconds(animTimeMap["anim_player_land"], () => {
                        isAnimationPlaying = false;
                    }));
                    break;
                case PlayerAnimationState.MeeleAttack:
                    AudioManager.instance.Play("Punch");
                    playerAnimator.Play("anim_player_punch");
                    isAnimationPlaying = true;
                    StartCoroutine(runAfterSeconds(animTimeMap["anim_player_punch"], () => {
                        isAnimationPlaying = false;
                    }));
                    break;
                case PlayerAnimationState.JumpMeeleAttack:
                    AudioManager.instance.Play("Punch");
                    playerAnimator.Play("anim_player_punch_vert");
                    isAnimationPlaying = true;
                    StartCoroutine(runAfterSeconds(animTimeMap["anim_player_punch_vert"], () => {
                        isAnimationPlaying = false;
                    }));
                    break;
                case PlayerAnimationState.Death:
                    playerAnimator.Play("anim_player_death");
                    isAnimationPlaying = true;
                    StartCoroutine(runAfterSeconds(animTimeMap["anim_player_death"], () => {
                        isAnimationPlaying = false;
                        GameManager.getInstance().GameOver();
                    }));
                    break;
            }
        }
    }

    private bool canInterruptAnimation(PlayerAnimationState animationState) {
        return animationState != PlayerAnimationState.MeeleAttack && animationState != PlayerAnimationState.JumpMeeleAttack;
    }

    private IEnumerator runAfterSeconds(float seconds, Action execute) {
        yield return new WaitForSeconds(seconds);
        execute();
    }

    public bool IsAbilityAvailable(AbilityType ability) {
        bool toReturn = false;
        try {
            toReturn = GameManager.getInstance().abilityMap[ability];
        } catch (System.Exception) {
            toReturn = false;
        }
        return toReturn;
    }
}
