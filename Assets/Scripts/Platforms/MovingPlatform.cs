﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    [SerializeField] private bool isMovingType = false;
    [SerializeField] private bool isKillable = false;
    private bool moveEnabled = true;
    [SerializeField] private bool isRangeEnabled = false;
    [SerializeField] private Vector2[] positions;
    [SerializeField] private float speed = 10.0f;
    private int positionIndexToReach = 0;
    private Rigidbody2D rb;

    void Start() {
        rb = gameObject.GetComponent<Rigidbody2D>();
        // disable this script if platform isn't a moving one
        if (!isMovingType) {
            gameObject.GetComponent<MovingPlatform>().enabled = false;
        }
        if (isRangeEnabled) moveEnabled = false;
    }

    void Update()
    {
        if (positions.Length > 0 && isMovingType && moveEnabled) {
            Vector2 currentPosition = (Vector2) gameObject.transform.position;

            if((currentPosition - positions[positionIndexToReach]).sqrMagnitude <= 0.1f) {
                positionIndexToReach = (positionIndexToReach + 1) % positions.Length;
            }
            
            Vector2 direction = (positions[positionIndexToReach] - currentPosition).normalized;
            rb.velocity = direction * speed;
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Player" && isRangeEnabled) {
            moveEnabled = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "Player" && isRangeEnabled) {
            moveEnabled = false;
            rb.velocity = Vector2.zero;
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == "Player" && isKillable) {
            moveEnabled = false;
            rb.velocity = Vector2.zero;
        }
    }
}
