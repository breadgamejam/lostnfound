using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

namespace Game_Jam.Assets.Scripts.Abilities
{
    public class Torch : Ability {
        private PlayerMovement playerMovement;
        private Light2D playerLight;
        void Start() {
            setDisplayName(AbilityType.Torch);
            playerMovement = gameObject.GetComponent<PlayerMovement>();
            playerLight = GetComponent<Light2D>();
            playerLight.enabled = false;
        }
        void Update()
        {
            if (playerMovement.IsAbilityAvailable(this.getDisplayName())) {
                playerLight.enabled = true;
            } else {
                playerLight.enabled = false;
            }
        }
    }
}