using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game_Jam.Assets.Scripts.Abilities
{
    public class Climb : Ability
    {
        [SerializeField] private float climbVelocity = 1f;
        [SerializeField] private float gravityScale = 1f;

        private PlayerMovement playerMovement;
        void Start() {
            playerMovement = gameObject.GetComponent<PlayerMovement>();
            setDisplayName(AbilityType.Climb);
        }

        // Update is called once per frame
        void Update() {
            if (playerMovement.IsAbilityAvailable(this.getDisplayName())) {
                GoUpOrDown();
            }
        }

        void GoUpOrDown() {
            Collider2D[] hitClimbableObjects = Physics2D.OverlapCircleAll(gameObject.transform.position, playerMovement.playerBehaviour.interactionRadius, playerMovement.playerBehaviour.climbableLayers);
            if (hitClimbableObjects.Length > 0) {
                if (Input.GetButton("Vertical")) {
                    if (Input.GetButton("Climb") && !(playerMovement.IsAbilityAvailable(AbilityType.Jump) && Input.GetButton("Jump"))) {
                        playerMovement.playerBehaviour.rb.gravityScale = 0f;
                        playerMovement.playerBehaviour.rb.velocity = new Vector2(playerMovement.playerBehaviour.rb.velocity.x, climbVelocity * Input.GetAxisRaw("Vertical"));
                    }
                } else {
                    if (Input.GetButton("Climb") && !(playerMovement.IsAbilityAvailable(AbilityType.Jump) && Input.GetButton("Jump"))) {
                        playerMovement.playerBehaviour.rb.gravityScale = 0f;
                        playerMovement.playerBehaviour.rb.velocity = new Vector2(playerMovement.playerBehaviour.rb.velocity.x, 0f);
                    } else {
                        Collider2D[] hitPlatformLayer = Physics2D.OverlapBoxAll(gameObject.transform.position, new Vector2(playerMovement.playerBehaviour.interactionRadius, playerMovement.playerBehaviour.interactionRadius * 2), playerMovement.playerBehaviour.platformLayerMask);
                        if (hitPlatformLayer.Length > 0) {
                            playerMovement.playerBehaviour.xVelocityCoefficient = 1f;
                        } else {
                            playerMovement.playerBehaviour.xVelocityCoefficient = 0f;
                        }
                        playerMovement.playerBehaviour.rb.gravityScale = gravityScale;
                    }
                }
            } else {
                playerMovement.playerBehaviour.rb.gravityScale = gravityScale;
            }
        }
    }
}