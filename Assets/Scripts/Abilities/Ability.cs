using System;
using UnityEngine;

namespace Game_Jam.Assets.Scripts.Abilities
{
    public class Ability : MonoBehaviour {
        private AbilityType displayName;
        public AbilityType getDisplayName() {
            return this.displayName;
        }

        public void setDisplayName(AbilityType displayName) {
            this.displayName = displayName;
        }
    }

    public enum AbilityType {
        Jump,
        MeleeAttack,
        Dash,
        Climb,
        Torch,
        None
    }
}