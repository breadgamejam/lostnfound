using System;
using UnityEngine;

namespace Game_Jam.Assets.Scripts.Abilities
{
    public class Jump : Ability
    {
        [SerializeField] private float jumpForce;

        private PlayerMovement playerMovement;
        void Start()
        {
            playerMovement = gameObject.GetComponent<PlayerMovement>();
            setDisplayName(AbilityType.Jump);
        }

        // Update is called once per frame
        void Update()
        {
            Collider2D[] hitClimbableObjects = Physics2D.OverlapCircleAll(gameObject.transform.position, playerMovement.playerBehaviour.interactionRadius, playerMovement.playerBehaviour.climbableLayers);
            if (playerMovement.IsAbilityAvailable(this.getDisplayName()) && (playerMovement.playerBehaviour.IsGrounded() || (playerMovement.IsAbilityAvailable(AbilityType.Climb) && Input.GetButton("Climb") && playerMovement.playerBehaviour.IsAir() && hitClimbableObjects.Length > 0)) && Input.GetButtonDown("Jump"))
            {
                playerMovement.isJumping = true;
                AudioManager.instance.Play("Jump");
                playerMovement.playerBehaviour.rb.velocity = new Vector2(playerMovement.playerBehaviour.rb.velocity.x, jumpForce);
            }
            else if (playerMovement.playerBehaviour.IsGrounded())
            {
                playerMovement.isJumping = false;
            }
        }
    }
}