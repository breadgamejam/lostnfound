using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game_Jam.Assets.Scripts.Abilities
{
    public class Dash : Ability
    {
        [SerializeField] private float dashTimer;
        [SerializeField] private float movementSpeed;
        private float dashFactor;
        private PlayerMovement playerMovement;
        private bool resetDashFactor = false;
        private float movementX;

        void Start() {
            playerMovement = gameObject.GetComponent<PlayerMovement>();
            dashFactor = 1;
            setDisplayName(AbilityType.Dash);
        }

        void Update()
        {
            // if (playerMovement.playerBehaviour.IsGrounded()) {
                movementX = Input.GetAxisRaw("Horizontal");
                if (movementX > 0) playerMovement.direction = 1; else if (movementX < 0) playerMovement.direction = -1;
                if (resetDashFactor) {
                    dashFactor = 1;
                    playerMovement.isDashEnabled = false;
                    resetDashFactor = false;
                }
            // }

            if (playerMovement.IsAbilityAvailable(this.getDisplayName()) && playerMovement.playerBehaviour.IsGrounded() && Input.GetButtonDown("Dash")) {
                dashFactor = 2.5f;
                playerMovement.isDashEnabled = true;
                StartCoroutine(runAfterSeconds(dashTimer, () => {
                    resetDashFactor = true;
                }));
            }
        }

        void FixedUpdate()
        {
            playerMovement.playerBehaviour.rb.velocity = new Vector2(playerMovement.playerBehaviour.xVelocityCoefficient * movementX * movementSpeed * dashFactor, playerMovement.playerBehaviour.rb.velocity.y);
        }

        private IEnumerator runAfterSeconds(float seconds, Action execute) {
            yield return new WaitForSeconds(seconds);
            execute();
        }
    }
}