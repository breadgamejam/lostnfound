using System;
using System.Collections;
using UnityEngine;

namespace Game_Jam.Assets.Scripts.Abilities
{
    public class MeleeAttack : Ability
    {
        [SerializeField] private Transform attackPointLeft;
        [SerializeField] private Transform attackPointRight;
        [SerializeField] private float attackRange = 0.5f;
        [SerializeField] private float attackCooldown = 0.5f;
        [SerializeField] private float jumpPunchOffset = 1f;
        [SerializeField] private LayerMask breakableObjectLayers;
        private bool attackAvailable = true;
        private PlayerMovement playerMovement;
        void Start() {
            playerMovement = gameObject.GetComponent<PlayerMovement>();
            setDisplayName(AbilityType.MeleeAttack);
        }

        // Update is called once per frame
        void Update() {
            if (playerMovement.IsAbilityAvailable(this.getDisplayName()) && Input.GetButtonDown("MeleeAttack") && attackAvailable) {
                attackAvailable = false;
                playerMovement.isPunching = true;
                StartCoroutine(runAfterSeconds(attackCooldown, () => {
                    attackAvailable = true;
                    playerMovement.isPunching = false;
                }));

                String animation;
                if (playerMovement.playerBehaviour.IsAir()) {
                    animation = "anim_player_punch_vert";
                } else {
                    animation = "anim_player_punch";
                }
                StartCoroutine(runAfterSeconds(playerMovement.animTimeMap[animation], () => {
                    Attack();
                }));
            }
        }

        void Attack() {
            Collider2D[] hitBreakableObjects = null;
            float jumpPunch = 0.0f;
            if (playerMovement.isJumping || (Input.GetButton("Climb") && playerMovement.IsAbilityAvailable(AbilityType.Climb))) {
                jumpPunch += jumpPunchOffset;
            }
            if (playerMovement.direction > 0) {
                hitBreakableObjects = Physics2D.OverlapCircleAll(new Vector2(attackPointRight.position.x, attackPointRight.position.y + jumpPunch), attackRange, breakableObjectLayers);
            } else if (playerMovement.direction < 0) {
                hitBreakableObjects = Physics2D.OverlapCircleAll(new Vector2(attackPointLeft.position.x, attackPointLeft.position.y + jumpPunch), attackRange, breakableObjectLayers);
            }

            if (hitBreakableObjects != null) {
                foreach (Collider2D breakableObject in hitBreakableObjects) {
                    Destroy(breakableObject.gameObject);
                    AudioManager.instance.Play("Break");
                }
            }
        }

        void OnDrawGizmosSelected() {
            if (attackPointLeft == null || attackPointRight == null) return;

            Gizmos.DrawWireSphere(attackPointLeft.position, attackRange);
            Gizmos.DrawWireSphere(attackPointRight.position, attackRange);
            Gizmos.DrawWireSphere(new Vector2(attackPointLeft.position.x, attackPointLeft.position.y + jumpPunchOffset), attackRange);
            Gizmos.DrawWireSphere(new Vector2(attackPointRight.position.x, attackPointRight.position.y + jumpPunchOffset), attackRange);
        }

        private IEnumerator runAfterSeconds(float seconds, Action execute) {
            yield return new WaitForSeconds(seconds);
            execute();
        }
    }
}