namespace Game_Jam.Assets.Scripts.Constants
{
    public enum PlayerAnimationState
    {
        Idle,
        Walk,
        FreeFall,
        ClimbUp,
        ClimbDown,
        StcukOnWall,
        Land,
        MeeleAttack,
        JumpMeeleAttack,
        Jump,
        Death
    }
}