﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game_Jam.Assets.Scripts.Abilities;

public class LevelConstants : MonoBehaviour
{
    [SerializeField] int numberAbilitySlots;
    [SerializeField] AbilityType[] initialActiveAbilityTypes;
    
    [SerializeField] public int currentLevel = -1;
    // [SerializeField] public int currentLevelBonus = -1;
    [SerializeField] public int currentLevelBonusMax = -1;
    // Start is called before the first frame update
    void Start()
    {
        // Debug.Assert(numberAbilitySlots <= initialActiveAbilityTypes.Length, "Number of ability slots should be greater or equal to than intial ability types");
        Debug.Assert(currentLevel != -1 && currentLevelBonusMax != -1, "Values of LevelConstants are still default!");
        GameManager.getInstance().currentLevel = currentLevel;
        GameManager.getInstance().currentLevelBonus = 0;
        GameManager.getInstance().currentLevelBonusMax = currentLevelBonusMax;
        GameManager.getInstance().abilitySlots = numberAbilitySlots;
        GameManager.getInstance().enabledAbilities = initialActiveAbilityTypes.Length;
        GameManager.getInstance().abilityMap = new Dictionary<AbilityType, bool>() {
            {AbilityType.Dash, false},
            {AbilityType.Jump, false},
            {AbilityType.MeleeAttack, false},
            {AbilityType.Torch, false},
            {AbilityType.Climb, false}
        };

        for(int i = 0; i < 3; i++)
        {
            GameManager.getInstance().abilitySlotsForGui[i] = AbilityType.None;
        }
        int j = 0;
        foreach (AbilityType abilityType in initialActiveAbilityTypes) {
            GameManager.getInstance().abilityMap[abilityType] = true;
            GameManager.getInstance().abilitySlotsForGui[j++] = abilityType;
        }
    }

    // // Update is called once per frame
    // void Update()
    // {
        
    // }
}
