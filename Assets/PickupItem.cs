﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game_Jam.Assets.Scripts.Abilities;

public class PickupItem : MonoBehaviour
{
    public GameObject jumpGameObject;
    public GameObject dashGameObject;
    public GameObject meleeAttackGameObject;
    public GameObject climbGameObject;
    public GameObject torchGameObject;
    AbilityType abilityToSwap = AbilityType.None;
    AbilityType currentAbility = AbilityType.None;
    AbilityType abilityToReplace = AbilityType.None;
    bool abilitySwapped = true;
    bool pressedOnce = false;


    private void Update() {
        if (!abilitySwapped) {
            if (GameManager.getInstance().abilitySlotsFilled()) {
                if (Input.GetButtonDown("PickupAbility") && !pressedOnce) {
                    string abilityInfo = "";
                    switch (currentAbility) {
                        case AbilityType.Jump:
                            abilityInfo = "This ability grants you the option to jump while on ground. This ability is activated by using \"SPACE\"";
                            break;
                        case AbilityType.Dash:
                            abilityInfo = "This ability grants you the option to run reallly fast for a short period of time. This ability is activated by using \"SHIFT\"";
                            break;
                        case AbilityType.MeleeAttack:
                            abilityInfo = "This ability grants you the option to kill enemies and break through certain walls. Use this ability while jumping to punch in upward direction [Jump ability should be enabled for upward punch]. This ability is activated by using \"E\"";
                            break;
                        case AbilityType.Climb:
                            abilityInfo = "This ability grants you the option to climb up and down on certain walls. You can also use jump while climbing [Jump ability should be enabled]. This ability is activated by using \"C\"";
                            break;
                    }

                    pressedOnce = true;
                    GUIManager.instance.swapAbility(currentAbility, abilityInfo, (AbilityType ability, int index) => {
                        if (ability != AbilityType.None && index > -1) {
                            abilityToReplace = ability;
                            GameManager.getInstance().enableAbility(abilityToReplace, currentAbility, index);
                            AudioManager.instance.Play("Ability");
                            switch (abilityToReplace) {
                                case AbilityType.Jump:
                                    Instantiate(jumpGameObject, gameObject.transform.position, Quaternion.identity);
                                    break;
                                case AbilityType.Dash:
                                    Instantiate(dashGameObject, gameObject.transform.position, Quaternion.identity);
                                    break;
                                case AbilityType.MeleeAttack:
                                    Instantiate(meleeAttackGameObject, gameObject.transform.position, Quaternion.identity);
                                    break;
                                case AbilityType.Climb:
                                    Instantiate(climbGameObject, gameObject.transform.position, Quaternion.identity);
                                    break;
                                case AbilityType.Torch:
                                    Instantiate(torchGameObject, gameObject.transform.position, Quaternion.identity);
                                    break;
                            }
                            abilitySwapped = true;
                            pressedOnce = false;
                            Destroy(gameObject);
                        } else {
                            pressedOnce = false;
                        }
                    });
                }
            } else {
                abilityToReplace = AbilityType.None;
                GameManager.getInstance().enableAbility(abilityToReplace, currentAbility, -1);
                abilitySwapped = true;
                AudioManager.instance.Play("Ability");
                Destroy(gameObject);
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Player") {
            switch (gameObject.tag) {
                case "AbilitySlotCollectible":
                    AudioManager.instance.Play("Ability");
                    GameManager.getInstance().incrementSlots();
                    Destroy(gameObject);
                    return;
                case "BonusCollectible":
                    GameManager.getInstance().incrementBonus();
                    AudioManager.instance.Play("Bonus");
                    Destroy(gameObject);
                    return;
                case "JumpAbilityCollectible":
                    currentAbility = AbilityType.Jump;
                    break;
                case "DashAbilityCollectible":
                    currentAbility = AbilityType.Dash;
                    break;
                case "MeleeAttackAbilityCollectible":
                    currentAbility = AbilityType.MeleeAttack;
                    break;
                case "ClimbAbilityCollectible":
                    currentAbility = AbilityType.Climb;
                    break;
                case "TorchCollectible":
                    currentAbility = AbilityType.Torch;
                    break;
            }
            if (!GameManager.getInstance().abilityMap[currentAbility]) abilitySwapped = false;
            if (GameManager.getInstance().abilitySlotsFilled() && !GameManager.getInstance().abilityMap[currentAbility]) {
                gameObject.GetComponentInChildren<Canvas>().enabled = true;
            } else {
                gameObject.GetComponentInChildren<Canvas>().enabled = false;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        abilitySwapped = true;
        try {
            gameObject.GetComponentInChildren<Canvas>().enabled = false;
        } catch (Exception e) {

        }
    }
}