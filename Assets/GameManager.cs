﻿using System.Collections.Generic;
using Game_Jam.Assets.Scripts.Abilities;
using UnityEngine;

public class GameManager
{
    private static GameManager instance;
    public Dictionary<AbilityType, bool> abilityMap;
    public int abilitySlots;
    public int enabledAbilities;
    public int currentLevel;
    public int currentLevelBonus;
    public int currentLevelBonusMax;
    public bool gamePaused;

    public AbilityType[] abilitySlotsForGui = {AbilityType.None, AbilityType.None, AbilityType.None};
    private GameManager()
    {
        abilityMap = new Dictionary<AbilityType, bool>() {
            {AbilityType.Dash, false},
            {AbilityType.Jump, false},
            {AbilityType.MeleeAttack, false},
            {AbilityType.Torch, false},
            {AbilityType.Climb, false}
        };
        // abilitySlots = 2;
        // enabledAbilities = 2;
        // currentLevel = 0;
        // currentLevelBonus = 0;
        // currentLevelBonusMax = 0;
        gamePaused = false;
    }

    public static GameManager getInstance()
    {
        if (instance == null)
        {
            instance = new GameManager();
        }
        return instance;
    }

    public void GameOver()
    {
        Time.timeScale = 0;
        GUIManager.instance.ShowGameOverPanel();
    }

    public void incrementSlots()
    {
        abilitySlots += 1;
    }

    public void incrementBonus()
    {
        currentLevelBonus += 1;
    }

    public void setMaxBonusPointsForLevel(int maxBonusPointsForLevel) {
        currentLevelBonusMax = maxBonusPointsForLevel;
    }

    public bool abilitySlotsFilled() {
        return enabledAbilities >= abilitySlots;
    }

    public void enableAbility(AbilityType ability, AbilityType replaceWithAbility, int index)
    {
        // empty slots available
        if (ability == AbilityType.None) {
            if (enabledAbilities + 1 <= abilitySlots) {
                enabledAbilities += 1;
                abilityMap[replaceWithAbility] = true;
                abilitySlotsForGui[enabledAbilities-1] = replaceWithAbility;
            }
            // else {
                //not enough slots available
            // }
        } else { // No empty slots and we have to replace ability
            if (!abilityMap[replaceWithAbility]) {
                abilityMap[replaceWithAbility] = true;
                abilityMap[ability] = false;
                abilitySlotsForGui[index] = replaceWithAbility;
            }
            // else {
                // ability to replace is already present, impossible case
            // }
        }
    }

    public void LevelComplete()
    {
        PlayerPrefs.SetInt("levelBonus" + currentLevel, currentLevelBonus);
        PlayerPrefs.SetInt("abilitySlots", abilitySlots);
        GUIManager.instance.levelCompletePanel.SetActive(true);
        // currentLevel += 1; //After load next level
        // currentLevelBonus = 0;
    }

    public bool PauseGame()
    {
        gamePaused = !gamePaused;
        Time.timeScale = gamePaused ? 0 : 1;
        return gamePaused;
    }

}