﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWall : MonoBehaviour
{
    [SerializeField] private bool isMovingType = false;
    [SerializeField] private bool isKillable = false;
    private bool moveEnabled = true;
    [SerializeField] private bool isRangeEnabled = false;
    [SerializeField] private Vector3[] positions;
    [SerializeField] private float speed = 10.0f;
    private int positionIndexToReach = 0;

    void Start()
    {
        if (!isMovingType)
        {
            gameObject.GetComponent<MovingPlatform>().enabled = false;
        }
        if (isRangeEnabled) moveEnabled = false;
    }

    void Update()
    {
        if (positions.Length > 0 && isMovingType && moveEnabled)
        {
            Vector3 currentPosition = (Vector3) gameObject.transform.position;

            if ((currentPosition - positions[positionIndexToReach]).sqrMagnitude <= 0.1f)
            {
                positionIndexToReach = (positionIndexToReach + 1) % positions.Length;
            }

            Vector3 direction = (positions[positionIndexToReach] - currentPosition).normalized;
            transform.position += direction * speed * Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && isRangeEnabled)
        {
            moveEnabled = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && isRangeEnabled)
        {
            moveEnabled = false;
            transform.position = positions[0];
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player" && isKillable)
        {
            moveEnabled = false;
        }
    }
}